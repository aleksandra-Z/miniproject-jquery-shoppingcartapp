$(document).ready(function()
{ 
    $(".book").hover(function(){
       $(".desc", this).fadeIn();
    }, function(){
       $(".desc", this).fadeOut(); 
    })
    .dragstart(function(event){
        event.dataTransfer.setData("text", $(this).attr("id"));
       $("#dragging-info").fadeIn();
    })
    .dragend(function(){
       $("#dragging-info").fadeOut();
    });
    
    $("#cart").dragover(function(event)
    {
       event.preventDefault();
       $("#cart").css("opacity","1");
    })
    .dragleave(function(event){
        $("#cart").css("opacity","0.7");
    })
    .drop(function(event){
    
        event.preventDefault();
        $(this).css("opacity","0.7");

        var bookId = event.dataTransfer.getData("text");
        var bookTitle = $("#"+bookId+" .title").text();
        var bookPrice = $("#"+bookId+" .price").text();
        var li = "<li class='chosen-book'><b>"+bookTitle+"</b><span class='chosen-book-price'>"+bookPrice+ "</span></li>";
        $("#chosen-books").append(li);
        var sum=0;
        $("#chosen-books .chosen-book-price").each(function(){
            sum+=parseFloat($(this).text());
        });
        $("#total-price span").text(sum.toFixed(2)+ "zł");
    });
});